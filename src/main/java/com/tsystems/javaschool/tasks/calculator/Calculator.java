package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private static String operators = "+-*/";
    private static String delimiters = "()" + operators;
    private static String numbers = "0123456789.";
    public static boolean flag = true;

    public String evaluate(String statement) {
        List<String> resultInPoliz = parse(statement);
        if (!flag) return null;
        double result = calc(resultInPoliz);
        if(flag) {
            if ((int) result == result)
                return Integer.toString((int)result);
            else if (((result * 10000) % 10) != 0)
                return String.format("%.4f", result).replace(',', '.');
            else if (((result * 1000) % 10) != 0)
                return String.format("%.3f", result).replace(',', '.');
            else if (((result * 100) % 10) != 0)
                return String.format("%.2f", result).replace(',', '.');
            else if (((result * 10) % 10) != 0)
                return String.format("%.1f", result).replace(',', '.');
            else return Double.toString(result);
        }
        else return null;
    }

    private static boolean isDelimiter(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isOperator(String token) {
        if (token.equals("u-")) return true;
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isNumber(String token) {
        boolean correct = false;
        int numperOfPoints = 0;
        for (int i = 0; i < token.length(); i++) {
            for (int j = 0; j < numbers.length(); j++) {
                if (token.charAt(i) == numbers.charAt(j)) {
                    correct = true;
                    if (token.charAt(i) == '.') numperOfPoints++;
                }
            }
            if (!correct || numperOfPoints > 1) return false;
            correct = false;
        }
        return true;
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    public static List<String> parse(String infix) {
        List<String> postfix = new ArrayList<>();
        if(infix == null) {
            flag = false;
            return postfix;
        }
        Deque<String> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
        String prev = "";
        String curr = "";
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                flag = false;
                return postfix;
            }
            else if (isDelimiter(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        postfix.add(stack.pop());
                        if (stack.isEmpty()) {
                            flag = false;
                            return postfix;
                        }
                    }
                    stack.pop();
                }
                else {
                    if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")"))))
                        curr = "u-";
                    else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek())))
                            postfix.add(stack.pop());
                    }
                    stack.push(curr);
                }
            }
            else if (isNumber(curr)) postfix.add(curr);
            else {
                flag = false;
                return postfix;
            }
            prev = curr;
        }
        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                flag = false;
                return postfix;
            }
        }
        return postfix;
    }

    public static Double calc(List<String> postfix) {
        Deque<Double> stack = new ArrayDeque<Double>();
        for (String x : postfix) {
            if (x.equals("+")) stack.push(stack.pop() + stack.pop());
            else if (x.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            }
            else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
            else if (x.equals("/")) {
                Double b = stack.pop(), a = stack.pop();
                if (b == 0) {
                    flag = false;
                }
                stack.push(a / b);
            }
            else if (x.equals("u-")) stack.push(-stack.pop());
            else stack.push(Double.valueOf(x));
        }
        return stack.pop();
    }
}