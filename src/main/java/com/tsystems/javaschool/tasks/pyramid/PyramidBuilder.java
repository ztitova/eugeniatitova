package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    private static int height;
    private static int width;

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(!checkException(inputNumbers)) throw new CannotBuildPyramidException();
        else {
            int[][] pyramid = new int [height][width];
            inputNumbers.sort(Comparator.naturalOrder());
            int leadingZeros = height - 1;
            int indexOfCurrNum = 0;
            for (int i = 0; i < height; i++, leadingZeros--)
                for (int j = leadingZeros; j < width - leadingZeros; j += 2, indexOfCurrNum++)
                    pyramid[i][j] = inputNumbers.get(indexOfCurrNum);
            return pyramid;
        }
    }

    private static boolean checkException(List<Integer> inputNumbers) {
        int count = inputNumbers.size();
        for (int i = 0; i < count; i++)
            if (inputNumbers.get(i) == null) return false;
        height = 0;
        while (count > 0) {
            height++;
            count -= height;
        }
        width = height * 2 - 1;
        if (count != 0) return false;
        else return true;
    }
}
