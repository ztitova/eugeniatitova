package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        else if (x.size() == 0) return true;
        else if (x.size() > y.size()) return false;
        else {
            Object prev = x.get(0);
            Object curr = null;
            for(int j = 0; j < y.indexOf(prev);) y.remove(j);
            for (Object i: x) {
                curr = i;
              if (y.contains(curr)) {
                  for (int j = y.indexOf(prev) + 1; j < y.indexOf(curr);) y.remove(j);
                  prev = curr;
              }
              else return false;
            }
            for (int k = y.indexOf(curr) + 1; k < y.size();) y.remove(k);
            if (x.equals(y)) return true;
            else return false;
        }
    }
}
